package models;

public class Person {
	private int arrivingTime, waitingTime, servingTime, id; 
	
	public Person(int id, int arrivingTime, int servingTime) {
		this.id = id;
		this.arrivingTime = arrivingTime;
		this.servingTime = servingTime;
	}
	
	public void increaseWaitingTime() {
		this.waitingTime++;
	}
	
	public void decreaseServingTime() {
		this.servingTime--;
	}

	public int getWaitingTime() {
		return waitingTime;
	}

	public int getServingTime() {
		return servingTime;
	}

	public int getArrivingTime() {
		return arrivingTime;
	}

	public int getId() {
		return id;
	}
}
