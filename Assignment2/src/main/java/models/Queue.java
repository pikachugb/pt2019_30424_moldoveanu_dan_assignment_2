package models;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Queue extends Thread implements Runnable{
	private BlockingQueue<Person> persons = new LinkedBlockingDeque<Person>();
	private int totalWaitingTime = 0;
	private int totalCustomersServed = 0;

	public BlockingQueue<Person> getPersons() {
		return persons;
	}
	
	public void increaseWaitingTimePersons() {
		for (Person person: persons) 
			person.increaseWaitingTime();
	}
	
	public int getTotalServingTime() {
		int total = 0;
		for (Person person: persons) 
			total += person.getServingTime();
		return total;
	}
	
	public int getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public int getTotalCustomersServed() {
		return totalCustomersServed;
	}

	@Override
	public String toString() {
		String p = "";
		for (Person person: persons) 
			p += "P" + person.getId() + "(" + person.getServingTime() + ") " ;
		return p;
	}
	
	
	@SuppressWarnings("static-access")
	@Override
	public void run() {
		while (true) {
			if (!persons.isEmpty()) {
				if ( persons.peek().getServingTime() > 0 ) {
					
					try {
						this.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					persons.peek().decreaseServingTime();
				}
				else {
					//System.out.println("P" + persons.peek().getId() + " left queue");
					totalWaitingTime += persons.peek().getWaitingTime();
					totalCustomersServed++;
					persons.poll();
				}
			}
		}
	}
}
