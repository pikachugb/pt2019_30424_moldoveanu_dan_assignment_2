package models;
public class MyTimer extends Thread {
	private volatile static int time = 0;
	private volatile boolean doStop = false;
    @Override
    
    public void run() {
    	while (!doStop) {
    		time++;
            System.out.println("\nTIME: " + time);
            try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
        
    }

	public int getTime() {
		return time;
	}
	
	public void pause() {
		doStop = true;
	}
	
	public void resetTime() {
		time = 0;
	}
}