package controller;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import models.MyTimer;
import models.Person;
import models.Queue;

public class SimulationManager implements Runnable {
	private static int minimumIntervalArrivingTime = 1, maximumIntervalArrivingTime = 2;
	private static int minimumServiceTime = 3, maximumServiceTime = 5;
	private static int numberOfQueues = 3;
	private static int simulationInterval = 10;
	private static int currentTime = 1;
	private static List<Person> clients = new ArrayList<Person>();
	private static List<Queue> queues = new ArrayList<Queue>();
	private static MyTimer timer = new MyTimer();
	private static Thread thread = new Thread(new SimulationManager());
	private static String log;
	private static String path;
	
	private Queue getMinimumServingTimeQueue() { 
		Queue minQueue = queues.get(0);
		for (Queue queue: queues)
			if (queue.getTotalServingTime() < minQueue.getTotalServingTime())
				minQueue = queue;
		return minQueue;
	}
	
	private void increaseAllWaitingTimes() {
		for (Queue queue: queues)
			queue.increaseWaitingTimePersons();
	}
	
	private static void generateClients() {
		int lastArrivingTime = -minimumIntervalArrivingTime + 1;
		int arrivingTime = 0, servingTime = 0;
		int simulationIntervalCopy = simulationInterval;
		int personNo = 1;
		while ( simulationInterval > 0 ) {
			Random random = new Random();
			arrivingTime = random.nextInt( maximumIntervalArrivingTime - minimumIntervalArrivingTime + 1 ) + minimumIntervalArrivingTime;
			servingTime = random.nextInt( maximumServiceTime - minimumServiceTime + 1 ) + minimumServiceTime;
			lastArrivingTime += arrivingTime;
			if ( simulationIntervalCopy > lastArrivingTime )
				clients.add(new Person(personNo, lastArrivingTime, servingTime));		
			else {
				clients.add(new Person(personNo, simulationIntervalCopy, servingTime));
				break;
			}
			personNo++;
			simulationInterval -= arrivingTime;
		}
		simulationInterval = simulationIntervalCopy;
	}
	
	private static boolean allQueuesAreEmpty() {
		for (Queue queue: queues)
			if (!queue.getPersons().isEmpty())
				return false;
		return true;
	}
	
	private static void printQueues() {
		for (Queue queue: queues) {
			log += "Q" + queue.getId() + ": " + queue.toString() + "\n";
			System.out.println("Q" + queue.getId() + ": " + queue.toString());
		}
	}
	
	private static void print() {
		for (int i = 0; i < clients.size() ; i++) {
			log += "P" + clients.get(i).getId() + " with arriving time " + clients.get(i).getArrivingTime() + " and serving time " + clients.get(i).getServingTime() + "\n";
			System.out.println("P" + clients.get(i).getId() + " with arriving time " + clients.get(i).getArrivingTime() + " and serving time " + clients.get(i).getServingTime());
		}
	}
	
	private static boolean checkClients() {
		if ( clients.size() == 0 )
			return true;
		if ( clients.size() != 0 && clients.get(0).getArrivingTime() != currentTime )
			return true;
		return false;
	}
	
	public static void start() {
		
		try {
			path = new File(".").getCanonicalPath() + "\\src";
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		generateClients();
		System.out.println("Number of clients: " + clients.size());
		log = "Number of clients: " + clients.size() + "\n";
		print();
		
		log += "\n TIME: 1\n";
		
		for (int i = 0; i < numberOfQueues; i++) {
			queues.add(new Queue());
			queues.get(i).start();
		}
		
		timer.start();
		thread.start();
	}

	@SuppressWarnings({ "deprecation" })
	public void run() {
		while ( timer.getTime() <= simulationInterval || !allQueuesAreEmpty()) {
			while ( clients.size() != 0 && clients.get(0).getArrivingTime() == timer.getTime() ) {			
				try {
					Queue minWaitTimeQueue = getMinimumServingTimeQueue();
					minWaitTimeQueue.getPersons().put(clients.get(0));
					System.out.println("P" + clients.get(0).getId() + " added to Q" + minWaitTimeQueue.getId());
					log += "P" + clients.get(0).getId() + " added to Q" + minWaitTimeQueue.getId() + "\n";
					clients.remove(0);
				} catch (InterruptedException e) {
					e.printStackTrace();			
				}
			}	
			if ( currentTime == timer.getTime() && checkClients() ) {
				printQueues();
				currentTime++;
				log += "\n TIME: " + currentTime + "\n";
				increaseAllWaitingTimes();
			}
		}
		
		timer.pause();
		System.out.println("Simulation ended!");
		log += "\nSimulation ended!";
		
		File file = new File(path + "\\log.txt");
		
	    BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(log);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Queue queue: queues) {
			queue.stop();
		}
	}

	public static int getMinimumIntervalArrivingTime() {
		return minimumIntervalArrivingTime;
	}

	public static int getMaximumIntervalArrivingTime() {
		return maximumIntervalArrivingTime;
	}

	public static int getMinimumServiceTime() {
		return minimumServiceTime;
	}

	public static int getMaximumServiceTime() {
		return maximumServiceTime;
	}

	public static int getNumberOfQueues() {
		return numberOfQueues;
	}

	public static int getSimulationInterval() {
		return simulationInterval;
	}

	public static void increaseMinimumIntervalArrivingTime() {
		if (minimumIntervalArrivingTime < maximumIntervalArrivingTime)
			minimumIntervalArrivingTime++;
	}
	public static void increaseMaximumIntervalArrivingTime() {
			maximumIntervalArrivingTime++;
	}
	
	public static void decreaseMinimumIntervalArrivingTime() {
			if (minimumIntervalArrivingTime > 1)
				minimumIntervalArrivingTime--;
	}
	
	public static void decreaseMaximumIntervalArrivingTime() {
		if (minimumIntervalArrivingTime < maximumIntervalArrivingTime)
			maximumIntervalArrivingTime--;
	}
	
	public static void increaseMinimumServiceTime() {
		if (minimumServiceTime < maximumServiceTime)
			minimumServiceTime++;
	}
	public static void increaseMaximumServiceTime() {
			maximumServiceTime++;
	}
	
	public static void decreaseMinimumServiceTime() {
			if (minimumServiceTime > 1)
				minimumServiceTime--;
	}
	
	public static void decreaseMaximumServiceTime() {
		if (minimumServiceTime < maximumServiceTime)
			maximumServiceTime--;
	}
	
	public static void decreaseNumberOfQueues() {
		if (numberOfQueues > 1)
			numberOfQueues--;
	}
	
	public static void increaseNumberOfQueues() {
			numberOfQueues++;
	}
	
	public static void decreaseSimulationInterval() {
		if (simulationInterval > 5)
			simulationInterval--;
	}
	
	public static void increaseSimulationInterval() {
		simulationInterval++;
	}

	public static int getCurrentTime() {
		return currentTime;
	}

	public static List<Person> getClients() {
		return clients;
	}

	public static List<Queue> getQueues() {
		return queues;
	}
}
