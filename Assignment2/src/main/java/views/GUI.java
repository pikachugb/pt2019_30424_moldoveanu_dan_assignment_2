package views;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.SimulationManager;
import models.Person;

public class GUI extends JPanel implements ActionListener{

	public static final long serialVersionUID = 1L;
	private static GridBagConstraints gbc = new GridBagConstraints();
	private static SimulationManager simulationManager = new SimulationManager();
	private static GUI gui;

	private static JButton startButton = new JButton("START");
	
	private static JLabel timeLabel = new JLabel();
	private static JLabel arrivingTimeLabel = new JLabel();
	private static JLabel serviceTimeLabel = new JLabel();
	private static JLabel numberOfQueuesLabel = new JLabel();
	private static JLabel simulationIntervalLabel = new JLabel();
	private static JLabel clientsLabel = new JLabel();
	
	private static JLabel arrivingTimeLabelMin = new JLabel("        Min:");
	private static JLabel arrivingTimeLabelMax = new JLabel("   Max:");
	
	private static JLabel serviceTimeLabelMin = new JLabel("        Min:");
	private static JLabel serviceTimeLabelMax = new JLabel("   Max:");
	
	private static JButton arrivingTimeButtonUpMin = new JButton("↑");
	private static JButton arrivingTimeButtonDownMin = new JButton("↓");
	private static JButton arrivingTimeButtonUpMax = new JButton("↑");
	private static JButton arrivingTimeButtonDownMax = new JButton("↓");
	
	private static JButton serviceTimeButtonUpMin = new JButton("↑");
	private static JButton serviceTimeButtonDownMin = new JButton("↓");
	private static JButton serviceTimeButtonUpMax = new JButton("↑");
	private static JButton serviceTimeButtonDownMax = new JButton("↓");
	
	private static JButton numberOfQueuesButtonUp = new JButton("↑");
	private static JButton numberOfQueuesButtonDown = new JButton("↓");
	
	private static JButton simulationIntervalButtonUp = new JButton("↑");
	private static JButton simulationIntervalButtonDown = new JButton("↓");
	
	private static boolean simulationStarted = false;
	
	private static List<TextField> queuesTextFields = new ArrayList<TextField>();
	private static List<JLabel> waitingTimeLabels = new ArrayList<JLabel>();
	
	
	@SuppressWarnings("static-access")
	private static void updateLabels() {
		arrivingTimeLabel.setText("Interval arriving time: [ " + simulationManager.getMinimumIntervalArrivingTime() + ", " + simulationManager.getMaximumIntervalArrivingTime() + " ]");
		serviceTimeLabel.setText("Service time: [ " + simulationManager.getMinimumServiceTime() + ", " + simulationManager.getMaximumServiceTime() + " ]");
		numberOfQueuesLabel.setText("Number of queues: " + simulationManager.getNumberOfQueues());
		simulationIntervalLabel.setText("Simulation interval: " + simulationManager.getSimulationInterval());
	}
	
	@SuppressWarnings("static-access")
	private static void updateTextFields() {
		for ( int i = 0; i < simulationManager.getNumberOfQueues(); i++ ) {
			queuesTextFields.get(i).setText(simulationManager.getQueues().get(i).toString());
		}
	}
	
	@SuppressWarnings("static-access")
	private static void updateClientsLabel() {
		String clientString = "";
		for(Person person: simulationManager.getClients())
			clientString += "P" + person.getId() + "(" + person.getArrivingTime() + ") ";
		clientsLabel.setText("<html>Clients (arriving time): " + clientString + "</html>");
	}
	
	@SuppressWarnings("static-access")
	private static void generateTextFields() {
		gbc.insets = new Insets(10, 5, 5, 5);
		gbc.gridx = 0;
		gbc.gridwidth = 6;

		updateClientsLabel();
		
		for (int i = 0; i < simulationManager.getNumberOfQueues(); i++) {
			queuesTextFields.add(new TextField());
			queuesTextFields.get(i).setEnabled(false);
			queuesTextFields.get(i).setFont(new Font("Arial", 0, 20));
			gbc.gridy = 6 + i;
			gui.add(queuesTextFields.get(i),gbc);			
		}
	}
	
	@SuppressWarnings("static-access")
	private static void generateWaitingTimeLabels() {
		gbc.insets = new Insets(10, 5, 5, 5);
		gbc.gridx = 6;
		
		for (int i = 0; i < simulationManager.getNumberOfQueues(); i++) {
			waitingTimeLabels.add(new JLabel());
			waitingTimeLabels.get(i).setFont(new Font("Arial", 0, 15));
			gbc.gridy = 6 + i;
			gui.add(waitingTimeLabels.get(i),gbc);			
		}
	}
	
	@SuppressWarnings("static-access")
	private static void updateWaitingTimeLabels() {
		for (int i = 0; i < simulationManager.getNumberOfQueues(); i++) {
			double f = 0;
			if ( simulationManager.getQueues().get(i).getTotalCustomersServed() != 0 )
				f = (double) simulationManager.getQueues().get(i).getTotalWaitingTime() / simulationManager.getQueues().get(i).getTotalCustomersServed();
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			waitingTimeLabels.get(i).setText(df.format(f));
		}
	}
	
	
	public GUI() {
		setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 40, 90);
		//0
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(startButton,gbc);		
		startButton.addActionListener(this);
		
		gbc.insets = new Insets(5, 5, 40, 5);
		gbc.gridx = 6;
		gbc.gridy = 0;
		add(timeLabel,gbc);		
		gbc.insets = new Insets(5, 5, 5, 5);
		//1
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(arrivingTimeLabel,gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(arrivingTimeLabelMin,gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(arrivingTimeButtonUpMin,gbc);
		arrivingTimeButtonUpMin.addActionListener(this);
		
		gbc.gridx = 3;
		gbc.gridy = 1;
		add(arrivingTimeButtonDownMin,gbc);
		arrivingTimeButtonDownMin.addActionListener(this);
		
		gbc.gridx = 4;
		gbc.gridy = 1;
		add(arrivingTimeLabelMax,gbc);
		
		gbc.gridx = 5;
		gbc.gridy = 1;
		add(arrivingTimeButtonUpMax,gbc);
		arrivingTimeButtonUpMax.addActionListener(this);
		
		gbc.gridx = 6;
		gbc.gridy = 1;
		add(arrivingTimeButtonDownMax,gbc);
		arrivingTimeButtonDownMax.addActionListener(this);
		//2
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(serviceTimeLabel,gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(serviceTimeLabelMin,gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 2;
		add(serviceTimeButtonUpMin,gbc);
		serviceTimeButtonUpMin.addActionListener(this);
		
		gbc.gridx = 3;
		gbc.gridy = 2;
		add(serviceTimeButtonDownMin,gbc);
		serviceTimeButtonDownMin.addActionListener(this);
		
		gbc.gridx = 4;
		gbc.gridy = 2;
		add(serviceTimeLabelMax,gbc);
		
		gbc.gridx = 5;
		gbc.gridy = 2;
		add(serviceTimeButtonUpMax,gbc);
		serviceTimeButtonUpMax.addActionListener(this);
		
		gbc.gridx = 6;
		gbc.gridy = 2;
		add(serviceTimeButtonDownMax,gbc);
		serviceTimeButtonDownMax.addActionListener(this);
		//3
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(numberOfQueuesLabel,gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 3;
		add(numberOfQueuesButtonUp,gbc);
		numberOfQueuesButtonUp.addActionListener(this);
		
		gbc.gridx = 3;
		gbc.gridy = 3;
		add(numberOfQueuesButtonDown,gbc);
		numberOfQueuesButtonDown.addActionListener(this);
		//4
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(simulationIntervalLabel,gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 4;
		add(simulationIntervalButtonUp,gbc);
		simulationIntervalButtonUp.addActionListener(this);
		
		gbc.gridx = 3;
		gbc.gridy = 4;
		add(simulationIntervalButtonDown,gbc);
		simulationIntervalButtonDown.addActionListener(this);
		

		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 8;
		add(clientsLabel,gbc);
		
	}
	
	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		JButton sourceObject = (JButton) e.getSource();
		
		if (sourceObject == startButton) {
			generateTextFields();
			generateWaitingTimeLabels();
			simulationManager.start();
			simulationStarted = true;
			sourceObject.setEnabled(false);
		}
		
		if(sourceObject == arrivingTimeButtonUpMin)
			simulationManager.increaseMinimumIntervalArrivingTime();
		if(sourceObject == arrivingTimeButtonDownMin)
			simulationManager.decreaseMinimumIntervalArrivingTime();
		

		if(sourceObject == arrivingTimeButtonUpMax)
			simulationManager.increaseMaximumIntervalArrivingTime();
		if(sourceObject == arrivingTimeButtonDownMax)
			simulationManager.decreaseMaximumIntervalArrivingTime();
		
		if(sourceObject == serviceTimeButtonUpMin)
			simulationManager.increaseMinimumServiceTime();
		if(sourceObject == serviceTimeButtonDownMin)
			simulationManager.decreaseMinimumServiceTime();
		

		if(sourceObject == serviceTimeButtonUpMax)
			simulationManager.increaseMaximumServiceTime();
		if(sourceObject == serviceTimeButtonDownMax)
			simulationManager.decreaseMaximumServiceTime();
		
		if(sourceObject == numberOfQueuesButtonUp)
			simulationManager.increaseNumberOfQueues();
		if(sourceObject == numberOfQueuesButtonDown)
			simulationManager.decreaseNumberOfQueues();
		

		if(sourceObject == simulationIntervalButtonUp)
			simulationManager.increaseSimulationInterval();
		if(sourceObject == simulationIntervalButtonDown)
			simulationManager.decreaseSimulationInterval();
		
		updateLabels();
	}
	
	@SuppressWarnings("static-access")
	public static void main(String args[]) throws IOException {
		gbc.fill = GridBagConstraints.BOTH;
		gui = new GUI();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		updateLabels();
		
		Thread guiThread = new Thread(new Runnable() {
			public void run() {
				while(true) {
					timeLabel.setText("<html>TIME: " + ( simulationManager.getCurrentTime() - 1 ) + "</html>");
					if ( simulationStarted ) {
						updateTextFields();
						updateClientsLabel();
						updateWaitingTimeLabels();
					}
				}				
			}
		});
		guiThread.start();		
		
		frame.setTitle("Queue simulator");
		frame.setSize(500,700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.add(gui);
	}
}
